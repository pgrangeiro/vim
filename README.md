VIM Customizado
=====================

Arquivos para a customização do VIM

Overview
--------------------------------------------------------------------------------------
Esta customização inclui os seguintes plugins:

- [Ack][] 
[Ack]: https://github.com/mileszs/ack.vim.git
- [Command-t][] 
[Command-t]: https://github.com/wincent/Command-T.git
- [Fugitive][] 
[Fugitive]: http://github.com/tpope/vim-fugitive.git
- [Git][] 
[Git]: https://github.com/tpope/vim-git.git
- [Gundo][] 
[Gundo]: https://github.com/sjl/gundo.vim.git
- [IndentLine][] 
[IndentLine]: https://github.com/Yggdroot/indentLine
- [Makegreen][] 
[Makegreen]: https://github.com/reinh/vim-makegreen
- [Minibufexpl][] 
[Minibufexpl]: https://github.com/sontek/minibufexpl.vim.git
- [Nerdtree][] 
[Nerdtree]: https://github.com/vim-scripts/The-NERD-tree.git
- [Pep8][] 
[Pep8]: https://github.com/vim-scripts/pep8.git
- [Py.test][] 
[Py.test]: https://github.com/alfredodeza/pytest.vim.git
- [Pydoc][] 
[Pydoc]: https://github.com/fs111/pydoc.vim.git
- [Ropevim][] 
[Ropevim]: https://github.com/sontek/rope-vim.git
- [Snipmate][] 
[Snipmate]: https://github.com/msanders/snipmate.vim.git
- [Surround][] 
[Surround]: https://github.com/tpope/vim-surround.git
- [Tasklist][] 
[Tasklist]: https://github.com/vim-scripts/TaskList.vim.git
- [Pyflakes-pathogen][] 
[Pyflakes-pathogen]: https://github.com/mitechie/pyflakes-pathogen.git



Instalação
--------------------------------------------------------------------------------------
1. Em /home/usuario clonar este repositório na pasta .vim
2. Renomear o arquivo vimrc_backup para .vimrc
3. Mover o arquivo .vimrc para /home/usuario
4. Na pasta .vim executar

> git submodule init

> git submodule update
